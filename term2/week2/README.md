# Week 14

## Question 1
Integration is used in the `RigidBody::Step` and `InvertedPendulum::Step` method, and they use Euler's first derivative method.

## Question 2
As the value of delta time increases, the physics simulation becomes more and more unstable, becoming completely unusable above a value of 1 second.

## Question 3
Midpoint
```c#
Vector2D midVelocity = oldVelocity + (acceleration * 0.5f * dt)
result.NewVelocity = oldVelocity + acceleration * dt;
result.NewPosition = oldPos + (midVelocity * dt);
```

RK4
```c#
Vector2D k1 = oldVelocity + (acceleration * dt);
Vector2D k2 = oldVelocity + (k1 * 0.5f * dt);
Vector2D k3 = oldVelocity + (k2 * 0.5f * dt);
Vector2D k4 = oldVelocity + (k3 * dt);

result.NewPosition = oldPos + (k1 + (k2 * 2.0f) + (k3 * 2.0f) + k4) * (1.0f / 6.0f) * dt;
result.NewVelocity = oldVelocity + (acceleration * dt);
```

## Question 4
In the update, divide the delta time by the physics update time target and take the floor, and run the physics update with the desired time step that number of times, then store the remainder in a member variable. In the next update, add the remainder to the delta time and repeat the process.
