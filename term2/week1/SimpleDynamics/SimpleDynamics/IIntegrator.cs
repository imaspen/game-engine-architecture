﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleDynamics
{
    struct IntegrationResult
    {
        public Vector2D NewPosition;
        public Vector2D NewVelocity;
    }

    interface IIntegrator
    {
        IntegrationResult Integrate(float dt, Vector2D oldPos, Vector2D oldVelocity, Vector2D acceleration);
    }
}
