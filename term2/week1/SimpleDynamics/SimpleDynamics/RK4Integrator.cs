﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleDynamics
{
    class RK4Integrator : IIntegrator
    {
        public IntegrationResult Integrate(float dt, Vector2D oldPos, Vector2D oldVelocity, Vector2D acceleration)
        {
            IntegrationResult result = new IntegrationResult();

            // f(tn, yn) where tn = dt and yn = acceleration
            // f(tn, yn) = 
            Vector2D k1 = oldVelocity + (acceleration * dt);
            Vector2D k2 = oldVelocity + (k1 * 0.5f * dt);
            Vector2D k3 = oldVelocity + (k2 * 0.5f * dt);
            Vector2D k4 = oldVelocity + (k3 * dt);

            result.NewPosition = oldPos + (k1 + (k2 * 2.0f) + (k3 * 2.0f) + k4) * (1.0f / 6.0f) * dt;
            result.NewVelocity = oldVelocity + (acceleration * dt);

            return result;
        }
    }
}
