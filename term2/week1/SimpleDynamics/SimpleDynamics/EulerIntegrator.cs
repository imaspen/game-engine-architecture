﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleDynamics
{
    class EulerIntegrator : IIntegrator
    {
        public IntegrationResult Integrate(float dt, Vector2D oldPos, Vector2D oldVelocity, Vector2D acceleration)
        {
            IntegrationResult result;

            result.NewVelocity = oldVelocity + acceleration * dt;
            result.NewPosition = oldPos + result.NewVelocity * dt;

            return result;
        }
    }
}
