﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleDynamics
{
    class RigidBody
    {
        public Vector2D Position { get; set; }              //Position of the centre of mass
        public Vector2D LinearVelocity { get; set; }        //linear velocity, for translational motion
        public Vector2D Force { get; set; }                 //body force
        public float AngularVelocity { get; set; }          //angular velocity, for rotational motion
        public float Mass { get; set; }
        public float InvMass { get; set; }
        public float Inertia { get; set; }
        public float InvInertia { get; set; }
        public bool IgnoreGravity { get; set; }
        public bool NBodySimulation { get; set; }
        public Shape Shape { get; set; }

        static IIntegrator integrator = new RK4Integrator();

        public RigidBody()
        {
            Mass = 200.0f;
            InvMass = 1.0f / Mass;
            Position = new Vector2D();
            LinearVelocity = new Vector2D();
            Force = new Vector2D();
            IgnoreGravity = false;
        }

        public void Step(float dt, World world)
        {
            Vector2D acceleration = Force * InvMass;
            if (!IgnoreGravity) acceleration += world.Gravity;
            if (NBodySimulation)
            {
                // FGrav = (G * m1 * m2) / r^2
                // AGrav * m2 = (G * m1 * m2) / r^2
                // AGrav = (G * m1) / r^2
                foreach (var rb in world.mDynamicPropList)
                {
                    if (!rb.NBodySimulation) continue;
                    if (rb == this) continue;
                    var dir = rb.Position - Position;
                    var magnitude = 1.0f / (float)Math.Sqrt(dir.LengthSqr());
                    acceleration += (dir * magnitude) * ((rb.Mass) / (dir.Length() * dir.Length()));
                }
            } 

            IntegrationResult result = integrator.Integrate(dt, Position, LinearVelocity, acceleration);

            Position = result.NewPosition;
            LinearVelocity = result.NewVelocity;
        }
    }
}
