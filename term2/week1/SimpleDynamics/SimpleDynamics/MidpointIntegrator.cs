﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleDynamics
{
    class MidpointIntegrator : IIntegrator
    {
        public IntegrationResult Integrate(float dt, Vector2D oldPos, Vector2D oldVelocity, Vector2D acceleration)
        {
            IntegrationResult result;

            Vector2D midVelocity = oldVelocity + (acceleration * 0.5f * dt)
            result.NewVelocity = oldVelocity + acceleration * dt;
            result.NewPosition = oldPos + (midVelocity * dt);

            return result;
        }
    }
}
