# Week 5 Exercises
## Task 1
### Question 1
A player can be determined to be a spectator using the `APlayerState::bIsSpectator` variable.

### Question 2
Player state is instantiated when a player is spawned and is managed by the game manager.

### Question 3
Player state can be retrieved from the player's `APawn::GetPlayerState()` class.

## Task 3
### Question 1
The game state class tracks the game mode, whether the game has begun, all player states, the timing of the world, the spectator class, and the match state.

### Question 2
The game state can be used by getting the world using `GetWorld()` and calling `UWorld::GetGameState()`.

