// Fill out your copyright notice in the Description page of Project Settings.

#include "BTSteeringService.h"
#include <EngineUtils.h>
#include <BehaviorTree/BlackboardComponent.h>
#include "VehicleTemplatePawn.h"
#include "AIWheeledVehicleController.h"

void UBTSteeringService::OnGameplayTaskActivated(UGameplayTask & task)
{
	//nothing here
	//This method must be overridden from the inherited interface
	//Otherwise, the linker will fail.
}

void UBTSteeringService::OnGameplayTaskDeactivated(UGameplayTask & task)
{
	//nothing here
	//This method must be overridden from the inherited interface
	//Otherwise, the linker will fail.
}

void UBTSteeringService::TickNode(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory, float DeltaSeconds)
{
	UWorld* world = OwnerComp.GetWorld();
	UBlackboardComponent * blackboard = OwnerComp.GetBlackboardComponent();
	TActorIterator<AVehicleTemplatePawn> PlayerPawnIter(world);
	//Not particularly robust, but at least here we are dealing with only one player
	PlayerPawn = *PlayerPawnIter;
	AActor * us = OwnerComp.GetOwner()->GetInstigator();

	FVector ourPos, theirPos, forward, right, diff;
	ourPos = us->GetActorLocation();
	theirPos = PlayerPawn->GetActorLocation();

	diff = theirPos - ourPos;
	diff.Normalize();

	forward = us->GetActorForwardVector();
	forward.Normalize();

	right = us->GetActorRightVector();
	right.Normalize();

	float angle1 = FMath::Acos(FVector::DotProduct(diff, forward));
	float angle2 = FMath::Acos(FVector::DotProduct(diff, right));

	FRotator ourRotation;
	ourRotation = us->GetActorRotation();

	float dist = FVector::Dist(theirPos, ourPos);
	float angleSpeedModifier = 1.0f - angle1 / (3.142f * 2);
	static float maxDist = 700.0f;
	float distanceSpeedModifier = FMath::Clamp<float>(FVector::Dist(theirPos, ourPos) - maxDist, 0.0f, maxDist) / maxDist;

	blackboard->SetValueAsFloat(TEXT("SteeringValue"), (angle2 < 1.571f ? 1.0f : -1.0f) * angle1);
	blackboard->SetValueAsFloat(TEXT("ThrottleValue"), 1.0f * angleSpeedModifier * distanceSpeedModifier);
}
