// Fill out your copyright notice in the Description page of Project Settings.


#include "FPSHUD.h"

#include <Engine/Canvas.h>
#include <GameFramework/PlayerController.h>

#include "MyPlayerState.h"

void AFPSHUD::DrawHUD()
{
	Super::DrawHUD();

	if (CrosshairTexture)
	{
		FVector2D Center(Canvas->ClipX * 0.5f, Canvas->ClipY * 0.5f);

		FVector2D CrosshairDrawPosition(Center.X - (CrosshairTexture->GetSurfaceWidth() * 0.5f), Center.Y - (CrosshairTexture->GetSurfaceHeight() * 0.5f));

		FCanvasTileItem TileItem(CrosshairDrawPosition, CrosshairTexture->Resource, FLinearColor::White);
		TileItem.BlendMode = SE_BLEND_Translucent;
		Canvas->DrawItem(TileItem);

		int points = GetOwningPlayerController()->GetPlayerState<AMyPlayerState>()->Points;
		FCanvasTextItem TextItem(FVector2D(), FText::Format(FTextFormat::FromString("{0} points."), points), CanvasFont, FLinearColor::Red);
		TextItem.Position = FVector2D(Canvas->ClipX - 300.0f, 10.0f);
		Canvas->DrawItem(TextItem);
	}
}
