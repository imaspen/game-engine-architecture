# Week 2 Exercises

## Task 1

### Question 1
We create the following component heirarchy in the `APawnWithCamera` constructor:

* `USceneComponent`
  * `USpringArmComponent`
    * `UCameraComponent`

### Question 2
Player input is handled by the following methods:
* `APawnWithCamera::MoveForward`
* `APawnWithCamera::MoveRight`
* `APawnWithCamera::PitchCamera`
* `APawnWithCamera::YawCamera`
* `APawnWithCamera::ZoomIn`
* `APawnWithCamera::ZoomOut`
They are bound in the `APawnWithCamera::SetupPlayerInputComponent` method and their values are used to update components in the `APawnWithCamera::Tick` method.

### Question 3
The `BindAction` method is passed callback functions using function pointers.

## Task 2

### Question 1
The `ACollidingPawn` class creates the following components in its constructor:
* `UCollidingPawnMovementComponent` Movement Manager
  * `USphereComponent` Root Component
    * `UStaticMeshComponent` Visual Sphere Mesh
      * `UParticleSystemComponent` Fire Particles
    * `USpringArmComponent` Camera Arm
      * `UCameraComponent` Camera

### Question 2
The `USphereComponent` handles collisions and the `UStaticMeshComponent` handles rendering.

### Question 3
`Super::Method()` calls the method defined in the class it inherits from, before running its own code.

### Question 4
The `SafeMoveUpdatedComponent` component takes a movement vector, a rotation, a sweep value that says whether to stop on collision, and a variable to store any hits in; then moves the object.

### Question 5
The movement component detects hits using the `Hit` variable, which is set by the `SafeMoveUpdatedComponent`. If `Hit.IsValidBlockingHit()` returns true, movement is prevented.

