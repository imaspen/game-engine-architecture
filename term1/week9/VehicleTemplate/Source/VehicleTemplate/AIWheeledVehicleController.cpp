// Fill out your copyright notice in the Description page of Project Settings.


#include "AIWheeledVehicleController.h"

#include <BehaviorTree/BehaviorTree.h>
#include <BehaviorTree/BehaviorTreeComponent.h>
#include <BehaviorTree/BlackboardComponent.h>
#include <WheeledVehicleMovementComponent.h>

#include "AIWheeledVehicle.h"

AAIWheeledVehicleController::AAIWheeledVehicleController()
{
	VehicleMovementComp = NULL;
	BehaviorTreeComp = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("AIVehicleBehaviorComp"));
	BlackboardComp = CreateDefaultSubobject<UBlackboardComponent>(TEXT("AIVehicleBlackboardComp"));
}

void AAIWheeledVehicleController::OnPossess(APawn * InPawn)
{
	Super::OnPossess(InPawn);
	AAIWheeledVehicle* AIVehicle = Cast<AAIWheeledVehicle>(InPawn);

	if (AIVehicle) VehicleMovementComp = AIVehicle->GetVehicleMovementComponent();

	StartBT(AIVehicle);
}

void AAIWheeledVehicleController::StartBT(AAIWheeledVehicle* AIVehicle)
{
	if (AIVehicle->BehaviorTree->BlackboardAsset) BlackboardComp->InitializeBlackboard(
		*(AIVehicle->BehaviorTree->BlackboardAsset)
	);
	BehaviorTreeComp->StartTree(*AIVehicle->BehaviorTree);
}

void AAIWheeledVehicleController::Tick(float DeltaSeconds)
{
	//VehicleMovementComp->SetThrottleInput(1.0f);
	//VehicleMovementComp->SetSteeringInput(-1.0f);
}
