// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "week10GameMode.generated.h"

UCLASS(minimalapi)
class Aweek10GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	Aweek10GameMode();
};



