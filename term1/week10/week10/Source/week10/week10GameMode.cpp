// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "week10GameMode.h"
#include "week10HUD.h"
#include "week10Character.h"
#include "UObject/ConstructorHelpers.h"

Aweek10GameMode::Aweek10GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = Aweek10HUD::StaticClass();
}
