// Fill out your copyright notice in the Description page of Project Settings.


#include "NPCCharacter.h"

#include <UObject/ConstructorHelpers.h>
#include <Engine/SkeletalMesh.h>
#include <Components/SkeletalMeshComponent.h>
#include <Animation/AnimSingleNodeInstance.h>
#include <Animation/BlendSpace1D.h>

// Sets default values
ANPCCharacter::ANPCCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	ConstructorHelpers::FObjectFinder<USkeletalMesh> SkeletonAsset(TEXT("/Game/MeleeDroid/Mesh/Melee_SkeletalMesh"));
	USkeletalMesh * SkeletalMesh = SkeletonAsset.Object;
	CachedMesh = this->GetMesh();
	CachedMesh->SetSkeletalMesh(SkeletalMesh);
	CachedMesh->AddLocalOffset(FVector(0.0f, 0.0f, -85.0f));

	ConstructorHelpers::FObjectFinder<UBlendSpace1D> BlendSpaceAsset(TEXT("/Game/MeleeDroid/Animations/IdleToRun"));
	blendSpaceOne = BlendSpaceAsset.Object;
}

// Called when the game starts or when spawned
void ANPCCharacter::BeginPlay()
{
	Super::BeginPlay();

	RunningTime = 0.0f;
	BlendValue = 0.0f;

	if (!CachedMesh) return;
	CachedMesh->PlayAnimation(blendSpaceOne, true);
	FVector BlendParams(BlendValue, 0.0f, 0.0f);
	CachedMesh->GetSingleNodeInstance()->SetBlendSpaceInput(BlendParams);
}

// Called every frame
void ANPCCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	RunningTime += DeltaTime;
	while (RunningTime > 10.0f) RunningTime -= 10.0f;

	BlendValue = FMath::Lerp<float, float>(0.0f, 100.0f, RunningTime / 3.0f);

	if (!CachedMesh) return;
	FVector BlendParams(BlendValue, 0.0f, 0.0f);
	CachedMesh->GetSingleNodeInstance()->SetBlendSpaceInput(BlendParams);
}

// Called to bind functionality to input
void ANPCCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

