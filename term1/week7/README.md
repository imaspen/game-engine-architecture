# Week 7 Exercise

Find the C++ equivalents to the blueprint functions used in the exercise.

## Enemy Controller
Run Behaviour Tree > `AAIController::RunBehaviorTree` 

Actor Has Tag > `AActor::ActorHasTag`

Break AIStimulus > Breaks out member variables of an `FAIStimulus`

Set Value as > `UBlackboardComponent::SetValue`

Set Timer by Event > `FTimerManager::SetTimer`

Clear and Invalidate Timer by Handle > `FTimerManager::ClearTimer`

## Tasks
Update Walk Speed, defined in blueprint.

Cast To > `Cast`

Finish Execute > Return an `EBTNodeResult` from a `UBTTaskNode::ExecuteTask` function

Set Blackboard Value as > `UBlackboardComponent::SetValue`

Get Actor Location > `AActor::GetActorLocation`

Get Random Reachable Point in Radius > `UNavigationSystemV1::GetRandomReachablePointInRadius`
