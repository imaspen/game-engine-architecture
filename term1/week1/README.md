# Week 1 Exercises

## Task 1

### Question 1
The `ACameraActor` class is derived from `AActor`.

### Question 2
`ACameraActor` has the following member variables:
* `AutoActiveForPlayer`
* `CameraComponent`
* `SceneComponent`
They are initialized in the editor.

### Question 3
The macro `UPROPERTY` allows fields to appear within the Unreal Editor.

### Question 4
An actor is something that can be put into a level.

There are many methods that can be overriden, some examples are:
* `UActorComponent::OnComponentCreated`
* `AActor::OnConstruction`
* `AActor::BeginPlay`

## Task 2

### Question 1
A Pawn is an object that can be controlled by a player or AI, it is a subclass of Actor.

### Question 2
There are many actor types already implemented, such as `CameraActor`, and for pawns there is also the `Character` class, which is a pawn with bipedal movement helpers.

### Question 3
Components are objects that can be attached to actors. They are derived as follows:
* `UObject`
  * `UActorComponent`: The most simple component, has no physical representation.
    * `USceneComponent`: Gains a location and rotation, and supports attachments.
      * `UPrimitiveComponent`: Gains world geomerty, useful for collision and rendering.

### Question 4
Within the constructor, the pawn has its `Tick` method enabled, is made to automatically recieve input from `Player0`, and has a camera and visual representation attached to it.

The constructor is called when the object is created, both within the editor and the game.
