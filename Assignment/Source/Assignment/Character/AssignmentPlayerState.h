// Copyright 2020 Aspen Thompson. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "AssignmentPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class ASSIGNMENT_API AAssignmentPlayerState : public APlayerState
{
	GENERATED_BODY()
	
};
