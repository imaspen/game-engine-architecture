// Copyright 2020 Aspen Thompson. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PawnMovementComponent.h"

#include "AssignmentPawnMovementComponent.generated.h"

USTRUCT()
struct ASSIGNMENT_API FAssignmentPawnSavedMovement
{
    GENERATED_BODY()

    FAssignmentPawnSavedMovement& operator+=(const FAssignmentPawnSavedMovement& RHS)
    {
        if (Invalid)
        {
            *this = RHS;
            return *this;
        }

        const auto Ticks = StoredTicks + RHS.StoredTicks;
        DeltaInput = (DeltaInput * StoredTicks + RHS.DeltaInput * RHS.StoredTicks) / Ticks;
        StoredTicks = Ticks;
        Rotation = RHS.Rotation;
        return *this;
    }

    UPROPERTY()
    uint32 StartTick;
    UPROPERTY()
    uint8 StoredTicks;
    UPROPERTY()
    FVector DeltaInput;
    UPROPERTY()
    FRotator Rotation;
    UPROPERTY()
    bool Invalid;
};

USTRUCT()
struct ASSIGNMENT_API FAssignmentPawnUpdate
{
    GENERATED_BODY()

    UPROPERTY()
    uint32 ClientTick;
    UPROPERTY()
    uint32 ServerTick;
    UPROPERTY()
    FVector Position;
    UPROPERTY()
    FRotator Rotation;
};

UCLASS()
class ASSIGNMENT_API UAssignmentPawnMovementComponent : public UPawnMovementComponent
{
    GENERATED_BODY()

public:
    void BeginPlay() override;

    void TickComponent(float DeltaTime, enum ELevelTick TickType,
                       FActorComponentTickFunction* ThisTickFunction) override;

    UFUNCTION()
    uint32 GetActiveServerTick() const;
    
    UFUNCTION()
    uint8 GetLerpTick() const;

    const TDoubleLinkedList<FAssignmentPawnUpdate>& GetAuthoritativeUpdates() const;

protected:
    UFUNCTION(Server, Reliable)
    void ServerAddMovement(FAssignmentPawnSavedMovement Movement);

    UFUNCTION(NetMulticast, Reliable)
    void MulticastUpdate(FAssignmentPawnUpdate Update);

    UPROPERTY(BlueprintReadWrite, EditAnywhere)
    float Speed = 500.0f;

    UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
    class AAssignmentPawn* AssignmentPawn;
    
private:
    void DoMove(FVector RotatedMovement, FRotator Rotation, float DeltaTime);
    void TickLocal();
    void TickRemote();
    void TickServer();

    FAssignmentPawnSavedMovement SavedMovementCache;
    TDoubleLinkedList<FAssignmentPawnSavedMovement> SavedMovements;

    TDoubleLinkedList<FAssignmentPawnUpdate> AuthoritativeUpdates;

    uint8 LerpTick;
    uint32 LastTick;
};
