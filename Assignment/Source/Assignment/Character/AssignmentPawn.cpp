// Copyright 2020 Aspen Thompson. All Rights Reserved.


#include "AssignmentPawn.h"


#include "Assignment/Blaster/BlasterComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Engine/Engine.h"
#include "GameFramework/GameStateBase.h"

AAssignmentPawn::AAssignmentPawn()
{
    PrimaryActorTick.bCanEverTick = true;

    CapsuleComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("RootComponent"));
    RootComponent = CapsuleComponent;

    CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));
    CameraComponent->SetupAttachment(RootComponent);

    BlasterComponent = CreateDefaultSubobject<UBlasterComponent>(TEXT("BlasterComponent"));

    MovementComponent = CreateDefaultSubobject<UAssignmentPawnMovementComponent>(TEXT("MovementComponent"));
}

// Called when the game starts or when spawned
void AAssignmentPawn::BeginPlay()
{
    Super::BeginPlay();

    RemoveTickPrerequisiteComponent(MovementComponent);

    CameraComponent->SetRelativeLocation(FVector(0.0f, 0.0f, BaseEyeHeight));
    TickNumber = 0;
}

// Called every frame
void AAssignmentPawn::Tick(const float DeltaTime)
{
    Super::Tick(DeltaTime);

    TickNumber++;
}

FVector AAssignmentPawn::GetCameraPosition() const
{
    return CameraComponent->GetComponentLocation();
}

UCapsuleComponent* AAssignmentPawn::GetCollisionComponent() const
{
    return CapsuleComponent;
}

UAssignmentPawnMovementComponent* AAssignmentPawn::GetMovementComponent() const
{
    return MovementComponent;
}

UBlasterComponent* AAssignmentPawn::GetBlasterComponent() const
{
    return BlasterComponent;
}

uint32 AAssignmentPawn::GetTickNumber() const
{
    return TickNumber;
}

void AAssignmentPawn::ClientReceiveHit_Implementation()
{
    if (GEngine)
        GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Red, TEXT("Ouch!"));
}
