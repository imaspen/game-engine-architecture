// Copyright 2020 Aspen Thompson. All Rights Reserved.

#pragma once

#include "AssignmentPawnMovementComponent.h"
#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "AssignmentPawn.generated.h"


UCLASS()
class ASSIGNMENT_API AAssignmentPawn : public APawn
{
    GENERATED_BODY()

public:
    // Sets default values for this pawn's properties
    AAssignmentPawn();

protected:
    UPROPERTY(BlueprintReadOnly, VisibleDefaultsOnly)
    class UCapsuleComponent* CapsuleComponent;

    UPROPERTY(BlueprintReadOnly, VisibleDefaultsOnly)
    class UCameraComponent* CameraComponent;

    UPROPERTY(BlueprintGetter=GetBlasterComponent, BlueprintReadOnly, VisibleDefaultsOnly)
    class UBlasterComponent* BlasterComponent;

    UPROPERTY(BlueprintReadOnly, VisibleDefaultsOnly)
    UAssignmentPawnMovementComponent* MovementComponent;

    void BeginPlay() override;

public:
    void Tick(float DeltaTime) override;

    UAssignmentPawnMovementComponent* GetMovementComponent() const override;

    UFUNCTION()
    FVector GetCameraPosition() const;

    UFUNCTION()
    UCapsuleComponent* GetCollisionComponent() const;

    UFUNCTION(BlueprintGetter)
    UBlasterComponent* GetBlasterComponent() const;

    UFUNCTION()
    uint32 GetTickNumber() const;

    UFUNCTION(Client, Reliable)
    virtual void ClientReceiveHit();

    UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category="Custom Replication")
    uint8 TicksPerSecond = 60;

    UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category="Custom Replication")
    uint8 TicksPerNetworkUpdate = 5;

    UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category="Custom Replication")
    float MaxLagCompensation = 5.0f;

    UFUNCTION()
    float GetTickDuration() const { return 1.0f / static_cast<float>(TicksPerSecond); }

    UFUNCTION()
    float GetNetworkUpdatesPerSecond() const
    {
        return static_cast<float>(TicksPerSecond) / static_cast<float>(TicksPerNetworkUpdate);
    }

private:
    uint32 TickNumber;
};


USTRUCT()
struct ASSIGNMENT_API FAssignmentPawnTickState
{
    GENERATED_BODY()

    FAssignmentPawnTickState() = default;

    explicit FAssignmentPawnTickState(AAssignmentPawn* AssignmentPawn)
    {
        const auto MovementComponent = CastChecked<UAssignmentPawnMovementComponent>(
            AssignmentPawn->GetMovementComponent()
        );
        Pawn = AssignmentPawn;
        ServerTick = MovementComponent->GetActiveServerTick();
        LerpTick = MovementComponent->GetLerpTick();
    }

    UPROPERTY()
    AAssignmentPawn* Pawn;
    UPROPERTY()
    uint32 ServerTick;
    UPROPERTY()
    uint8 LerpTick;
};
