// Copyright 2020 Aspen Thompson. All Rights Reserved.


#include "AssignmentPlayerController.h"


#include "AssignmentPawn.h"
#include "Assignment/Blaster/BlasterComponent.h"

void AAssignmentPlayerController::SetupInputComponent()
{
    Super::SetupInputComponent();

    check(InputComponent);
    if (InputComponent != nullptr)
    {
        InputComponent->BindAxis("MoveX", this, &AAssignmentPlayerController::MoveX);
        InputComponent->BindAxis("MoveY", this, &AAssignmentPlayerController::MoveY);
        InputComponent->BindAxis("LookX", this, &AAssignmentPlayerController::LookX);
        InputComponent->BindAxis("LookY", this, &AAssignmentPlayerController::LookY);
        InputComponent->BindAction("Fire", IE_Pressed, this, &AAssignmentPlayerController::Fire);
    }
}

void AAssignmentPlayerController::MoveX(const float Value)
{
    if (GetPawn() == nullptr) return;
    GetPawn()->AddMovementInput(FVector(1.0f, 0.0f, 0.0f), Value);
}

void AAssignmentPlayerController::MoveY(const float Value)
{
    if (GetPawn() == nullptr) return;
    GetPawn()->AddMovementInput(FVector(0.0f, 1.0f, 0.0f), Value);
}

void AAssignmentPlayerController::LookX(const float Value)
{
    if (GetPawn() == nullptr) return;
    GetPawn()->AddControllerYawInput(Value);
}

void AAssignmentPlayerController::LookY(const float Value)
{
    if (GetPawn() == nullptr) return;
    GetPawn()->AddControllerPitchInput(Value);
}

void AAssignmentPlayerController::Fire()
{
    const auto AssignmentPawn = GetPawn<AAssignmentPawn>();
    if (AssignmentPawn == nullptr) return;
    AssignmentPawn->GetBlasterComponent()->Fire();
}
