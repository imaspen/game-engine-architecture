// Copyright 2020 Aspen Thompson. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "AssignmentPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class ASSIGNMENT_API AAssignmentPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	void SetupInputComponent() override;

private:
	void MoveX(float Value);
	void MoveY(float Value);
	
	void LookX(float Value);
	void LookY(float Value);
	void Fire();
};
