// Copyright 2020 Aspen Thompson. All Rights Reserved.


#include "AssignmentPawnMovementComponent.h"

#include "AssignmentPawn.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/GameStateBase.h"
#include "Windows/WindowsApplication.h"

void UAssignmentPawnMovementComponent::BeginPlay()
{
    Super::BeginPlay();

    LerpTick = 0;

    AssignmentPawn = CastChecked<AAssignmentPawn>(PawnOwner);

    AddTickPrerequisiteActor(PawnOwner);
}

void UAssignmentPawnMovementComponent::TickComponent(const float DeltaTime, const ELevelTick TickType,
    FActorComponentTickFunction* ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

    if (!PawnOwner || !UpdatedComponent) return;

    switch (GetOwnerRole())
    {
        case ROLE_AutonomousProxy: TickLocal();
            break;
        case ROLE_SimulatedProxy: TickRemote();
            break;
        case ROLE_Authority: TickServer();
            break;
        default: ;
    }
}

uint32 UAssignmentPawnMovementComponent::GetActiveServerTick() const
{
    return AuthoritativeUpdates.Num() > 0
               ? AuthoritativeUpdates.GetHead()->GetValue().ServerTick
               : LastTick;
}

uint8 UAssignmentPawnMovementComponent::GetLerpTick() const
{
    return LerpTick;
}

const TDoubleLinkedList<FAssignmentPawnUpdate>& UAssignmentPawnMovementComponent::GetAuthoritativeUpdates() const
{
    return AuthoritativeUpdates;
}

void UAssignmentPawnMovementComponent::DoMove(const FVector RotatedMovement, const FRotator Rotation,
    const float DeltaTime)
{
    // Move the pawn, respecting collision
    FHitResult HitResult;
    SafeMoveUpdatedComponent(RotatedMovement * Speed * DeltaTime, Rotation, true, HitResult);
    if (HitResult.IsValidBlockingHit())
        SlideAlongSurface(
            RotatedMovement * Speed * DeltaTime, 1.0f - HitResult.Time, HitResult.Normal, HitResult
        );
}

void UAssignmentPawnMovementComponent::TickLocal()
{
    // Process received updates from the server
    if (AuthoritativeUpdates.Num() > 0)
    {
        const auto Update = AuthoritativeUpdates.GetTail()->GetValue();
        PawnOwner->SetActorLocation(Update.Position);

        // Drop moves older than our received processed move.
        while (SavedMovements.Num() > 0 && Update.ClientTick >= SavedMovements.GetHead()->GetValue().StartTick
        )
            SavedMovements.RemoveNode(SavedMovements.GetHead());

        // Replay unprocessed moves.
        for (const auto Move : SavedMovements)
            DoMove(
                Move.DeltaInput, Move.Rotation,
                AssignmentPawn->GetTickDuration() * AssignmentPawn->TicksPerNetworkUpdate
            );

        // Replay anything we have yet to send.
        if (!SavedMovementCache.Invalid)
            DoMove(
                SavedMovementCache.DeltaInput, PawnOwner->GetControlRotation(),
                AssignmentPawn->GetTickDuration() * SavedMovementCache.StoredTicks
            );

        AuthoritativeUpdates.Empty();
    }

    // Calculate input
    const auto Input = AssignmentPawn->ConsumeMovementInputVector();
    auto Rotation = AssignmentPawn->GetControlRotation();
    Rotation.Pitch = 0.0f;
    const auto Movement = Rotation.RotateVector(Input.GetClampedToMaxSize(1.0f));

    // Create and cache the movement
    FAssignmentPawnSavedMovement SavedMovement;
    SavedMovement.StartTick = AssignmentPawn->GetTickNumber();
    SavedMovement.StoredTicks = 1;
    SavedMovement.DeltaInput = Movement;
    SavedMovement.Rotation = Rotation;
    SavedMovement.Invalid = false;
    SavedMovementCache += SavedMovement;

    // Send the cache to the server if enough ticks have been accumulated
    if (SavedMovementCache.StoredTicks >= AssignmentPawn->TicksPerNetworkUpdate)
    {
        ServerAddMovement(SavedMovementCache);
        SavedMovements.AddTail(SavedMovementCache);
        SavedMovementCache.Invalid = true;
    }

    // Finally, do the move
    DoMove(Movement, Rotation, AssignmentPawn->GetTickDuration());
}

void UAssignmentPawnMovementComponent::TickRemote()
{
    // Check if we've finished applying a server update, reset the tick counts and move to the next node
    if (LerpTick >= AssignmentPawn->TicksPerNetworkUpdate)
    {
        LerpTick = 0;
        if (AuthoritativeUpdates.Num() > 0)
        {
            LastTick = AuthoritativeUpdates.GetHead()->GetValue().ServerTick;
            AuthoritativeUpdates.RemoveNode(AuthoritativeUpdates.GetHead());
        }
    }

    if (AuthoritativeUpdates.Num() > 0)
    {
        // Get the updates to lerp between
        const auto Current = AuthoritativeUpdates.GetHead()->GetValue();
        const auto Next = AuthoritativeUpdates.Num() > 1
                              ? AuthoritativeUpdates.GetHead()->GetNextNode()->GetValue()
                              : Current;

        // Update the actor's position
        PawnOwner->SetActorLocation(FMath::Lerp(
            Current.Position, Next.Position,
            static_cast<float>(LerpTick) / static_cast<float>(AssignmentPawn->TicksPerNetworkUpdate)
        ));

        PawnOwner->SetActorRotation(FMath::Lerp(
            Current.Rotation, Next.Rotation,
            static_cast<float>(LerpTick) / static_cast<float>(AssignmentPawn->TicksPerNetworkUpdate)
        ));

        LerpTick++;
    }
}

void UAssignmentPawnMovementComponent::TickServer()
{
    if (SavedMovements.Num() <= 0) return;

    // Apply the oldest received update
    auto& Move = SavedMovements.GetHead()->GetValue();
    DoMove(
        Move.DeltaInput, Move.Rotation,
        AssignmentPawn->GetTickDuration() * AssignmentPawn->TicksPerNetworkUpdate
    );

    // Inform others of the move
    MulticastUpdate({
        Move.StartTick, AssignmentPawn->GetTickNumber(),
        GetActorLocation(), AssignmentPawn->GetActorRotation()
    });

    // Remove applied update
    SavedMovements.RemoveNode(SavedMovements.GetHead());
}

void UAssignmentPawnMovementComponent::ServerAddMovement_Implementation(const FAssignmentPawnSavedMovement Movement)
{
    SavedMovements.AddTail(Movement);
}

void UAssignmentPawnMovementComponent::MulticastUpdate_Implementation(const FAssignmentPawnUpdate Update)
{
    // Store received update
    AuthoritativeUpdates.AddTail(Update);

    // Remove stale updates from the server
    if (GetOwnerRole() == ROLE_Authority)
        while (AuthoritativeUpdates.Num()
            > AssignmentPawn->GetNetworkUpdatesPerSecond() * AssignmentPawn->MaxLagCompensation)
        {
            AuthoritativeUpdates.RemoveNode(AuthoritativeUpdates.GetHead());
        }
}
