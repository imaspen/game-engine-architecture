// Copyright 2020 Aspen Thompson. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "AssignmentGameStateBase.generated.h"

/**
 * 
 */
UCLASS()
class ASSIGNMENT_API AAssignmentGameStateBase : public AGameStateBase
{
	GENERATED_BODY()
	
};
