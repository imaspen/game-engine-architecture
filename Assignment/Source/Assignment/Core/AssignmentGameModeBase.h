// Copyright 2020 Aspen Thompson. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "AssignmentGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ASSIGNMENT_API AAssignmentGameModeBase final : public AGameModeBase
{
	GENERATED_BODY()
	
};
