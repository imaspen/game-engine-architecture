// Copyright 2020 Aspen Thompson. All Rights Reserved.


#include "BlasterComponent.h"


#include "CollisionDebugDrawingPublic.h"
#include "EngineUtils.h"
#include "Assignment/Character/AssignmentPawn.h"
#include "Assignment/Character/AssignmentPawnMovementComponent.h"
#include "Containers/ArrayBuilder.h"
#include "Engine/Engine.h"
#include "Engine/World.h"

UBlasterComponent::UBlasterComponent()
{
    PrimaryComponentTick.bCanEverTick = false;
}

void UBlasterComponent::BeginPlay()
{
    Super::BeginPlay();

    AssignmentPawn = CastChecked<AAssignmentPawn>(GetOwner());

    AddTickPrerequisiteActor(GetOwner());
    AddTickPrerequisiteComponent(AssignmentPawn->GetMovementComponent());
}

void UBlasterComponent::Fire()
{
    UE_LOG(LogTemp, Display, TEXT("Fired"));
    if (!GetOwner()) return;
    if (!GetWorld()) return;

    // Get the local states of other pawns
    TArrayBuilder<FAssignmentPawnTickState> TickStates;
    for (auto* OtherPawn : TActorRange<AAssignmentPawn>(GetWorld()))
    {
        if (GetOwner() == OtherPawn) continue;
        TickStates.Add(FAssignmentPawnTickState(OtherPawn));
    }

    // Inform the server of our state and that we are firing
    ServerFire({ TickStates, AssignmentPawn->GetControlRotation() });

    // Calculate local hits
    if (GetHitPawn(AssignmentPawn->GetCameraPosition(), AssignmentPawn->GetControlRotation()) && GEngine)
    {
        LocalHits++;
        GEngine->AddOnScreenDebugMessage(
            1, 10.0f, FColor::Purple,
            FString::Printf(TEXT("Local Hits: %u, Remote Hits: %u"), LocalHits, RemoteHits)
        );
    }
}

void UBlasterComponent::ClientHit_Implementation()
{
    // Receive hit confirmation
    RemoteHits++;
    if (GEngine)
        GEngine->AddOnScreenDebugMessage(
            2, 10.0f, FColor::Green,
            FString::Printf(TEXT("Local Hits: %u, Remote Hits: %u"), LocalHits, RemoteHits)
        );
}

void UBlasterComponent::ClientMiss_Implementation()
{
    // Receive miss confirmation
    if (GEngine)
        GEngine->AddOnScreenDebugMessage(
            -1, 5.0f, FColor::Blue, TEXT("Missed!")
        );
}

AAssignmentPawn* UBlasterComponent::GetHitPawn(const FVector StartPos, const FRotator Rotation) const
{
    // Do the trace
    const auto Direction = Rotation.RotateVector(FVector::ForwardVector);
    const auto EndPos = StartPos + Direction * 10000.0f; // + 100 meters in look direction

    FHitResult HitResult;
    FCollisionQueryParams QueryParams(FName(TEXT("FireTrace")), true, GetOwner());

    GetWorld()->LineTraceSingleByProfile(HitResult, StartPos, EndPos, FName("BlockAll"), QueryParams);

    // Draw a debug line on the client
    if (GetOwnerRole() == ROLE_AutonomousProxy)
        DrawLineTraces(GetWorld(), StartPos, EndPos,
            TArray<FHitResult>{ HitResult }, 10.0f);

    // Return nullptr if no hit was found
    if (!HitResult.IsValidBlockingHit()) return nullptr;

    // Return the hit actor
    UE_LOG(LogTemp, Display, TEXT("Hit"));
    const auto HitPawn = Cast<AAssignmentPawn>(HitResult.GetActor());
    return HitPawn;
}

void UBlasterComponent::ServerFire_Implementation(FBlasterFireState FireState)
{
    UE_LOG(LogTemp, Display, TEXT("ServerFired"));

    // Roll back other pawns
    for (const auto& TickState : FireState.PawnTickStates)
    {
        auto* Pawn = TickState.Pawn;
        const auto* MovementComponent = CastChecked<UAssignmentPawnMovementComponent>(
            Pawn->GetMovementComponent()
        );
        const auto* FirstUpdate = MovementComponent->GetAuthoritativeUpdates().GetHead();

        // Skip if we have no updates
        if (FirstUpdate == nullptr) continue;

        // If the requested update has fallen out of the cache, use its last known position
        if (FirstUpdate->GetValue().ServerTick > TickState.ServerTick)
        {
            Pawn->SetActorLocation(FirstUpdate->GetValue().Position);
            UE_LOG(LogTemp, Display, TEXT("%s rollback out of range."), *Pawn->GetFName().ToString());
            continue;
        }

        // Find the update that matches the tick state
        for (auto Update = FirstUpdate;
             Update != nullptr;
             Update = Update->GetNextNode())
        {
            // Check the current update matches the tick state's update
            if (Update->GetValue().ServerTick != TickState.ServerTick) continue;

            // Interpolate the new position between the update and the next update.
            const auto Location = FMath::Lerp(
                Update->GetValue().Position,
                Update->GetNextNode()->GetValue().Position,
                static_cast<float>(TickState.LerpTick) / AssignmentPawn->TicksPerNetworkUpdate
            );
            Pawn->SetActorLocation(Location);

            UE_LOG(LogTemp, Display, TEXT("%s rolling back to %u."), *Pawn->GetFName().ToString(), Update->GetValue().ServerTick);
            break;
        }
    }

    // Check for hit
    const auto HitPawn = GetHitPawn(AssignmentPawn->GetCameraPosition(), FireState.Rotation);
    if (HitPawn == nullptr)
    {
        UE_LOG(LogTemp, Display, TEXT("Shot missed."));
        ClientMiss();
    }
    else
    {
        UE_LOG(LogTemp, Display, TEXT("Shot hit %s"), *HitPawn->GetFName().ToString());
        ClientHit();
        HitPawn->ClientReceiveHit();
    }

    //Reset the pawns
    for (const auto& TickState : FireState.PawnTickStates)
    {
        const auto* MovementComponent = CastChecked<UAssignmentPawnMovementComponent>(
            TickState.Pawn->GetMovementComponent()
        );
        TickState.Pawn->SetActorLocation(MovementComponent->GetAuthoritativeUpdates().GetTail()->GetValue().Position);
    }
}
