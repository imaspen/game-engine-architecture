// Copyright 2020 Aspen Thompson. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Assignment/Character/AssignmentPawn.h"
#include "Components/ActorComponent.h"

#include "BlasterComponent.generated.h"


USTRUCT()
struct FBlasterFireState
{
    GENERATED_BODY()

    UPROPERTY()
    TArray<FAssignmentPawnTickState> PawnTickStates;
    UPROPERTY()
    FRotator Rotation;
};


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ASSIGNMENT_API UBlasterComponent : public UActorComponent
{
    GENERATED_BODY()

public:
    // Sets default values for this component's properties
    UBlasterComponent();

    void BeginPlay() override;

    UFUNCTION(BlueprintCallable)
    virtual void Fire();

    UFUNCTION(Server, Reliable)
    virtual void ServerFire(FBlasterFireState FireState);

    UFUNCTION(Client, Reliable)
    virtual void ClientHit();

    UFUNCTION(Client, Reliable)
    virtual void ClientMiss();

protected:
    UPROPERTY()
    AAssignmentPawn* AssignmentPawn;

private:
    AAssignmentPawn* GetHitPawn(FVector StartPos, FRotator Rotation) const;

    uint8 LocalHits = 0;
    uint8 RemoteHits = 0;
    
};
