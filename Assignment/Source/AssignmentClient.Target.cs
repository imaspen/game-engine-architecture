// Insert Your Copyright here

using UnrealBuildTool;
using System.Collections.Generic;

public class AssignmentClientTarget : TargetRules
{
	public AssignmentClientTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Client;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.Add("Assignment");
	}
}
