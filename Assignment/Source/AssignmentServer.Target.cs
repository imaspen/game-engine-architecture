// Insert Your Copyright here

using UnrealBuildTool;
using System.Collections.Generic;

public class AssignmentServerTarget : TargetRules
{
	public AssignmentServerTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Server;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.Add("Assignment");
	}
}
